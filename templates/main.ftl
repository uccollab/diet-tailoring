<@compress single_line=true>
	<#include "/greetings/${random.pick()}.txt"> ${username},
</@compress>
<#-- ... -->
<#-- ... -->

<@compress single_line=true>
	<#include "/descrFeedback/${random.pick()}.txt"> "${motivation}", <#include "/raisExp/${random.pick()}.txt">
</@compress>
<#-- ... -->
<#-- ... -->


<@compress single_line=true>
    <#if calories ==  1>
        <#if cal_shift_significant == 0>
            <#include "/goodCalBehaviour/${random.pick()}.txt">
        <#else>
            <#include "/badCalBehaviour/${random.pick()}.txt">
        </#if>
    <#-- ... -->
        <#if cal_shift_significant==1>:
            <#if cal_shift gt 100>
                <#include "/overEating/${random.pick()}.txt">
            <#else> <#include "/underEating/${random.pick()}.txt">
            </#if>
            <#if (100-cal_shift)?round != 0>
                <#if low_numeracy == 0>
                    (generally you ate ${quantifier.scale(cal_shift?round, true, false)} your target).
                <#else> (generally you ate ${quantifier.paraphrase(cal_shift,false)} <#if cal_shift_significant ==1> your target</#if>).
                </#if>
            </#if>
        <#elseif precise_reporting == 1>
                <#if low_numeracy == 0>
                    (generally you ate just ${quantifier.scale(cal_shift?round, true, false)} your target).
                <#else> (generally you're eating the suggested amount).
                </#if>
        <#else>.
        </#if>

        <#if cal_shift_comp??>
            <#include "/comp/"+cal_shift_comp+"/${random.pick()}.txt">
        </#if>

        <#if (cal_fur_significant == 1 || precise_reporting == 1) && (quantifier.round(cal_fur_val) != 0)>
            <#include "/worstDay/${random.pick()}.txt">
            <#if (cal_fur_significant == 1 || precise_reporting == 1)>
                (you ate
                <#if low_numeracy == 1>
                    ${quantifier.paraphrase(cal_fur_val,false)}
                <#else>
                    ${quantifier.scale(cal_fur_val, true, false)}
                </#if> your target).
            </#if>
            <#else> All of your days looks similar.
        </#if>
    </#if>


</@compress>
<#-- ... -->
<#-- ... -->
<#if exercise == 1>


    <@compress single_line=true>
            <#include "/exercise/True/"+random.pick()+".txt">
    </@compress>
</#if>
    <#if exercise == 1>


    </#if>
<@compress single_line=true>
	<#if nutr_n gt 0>
        <#include "/badNutrBehaviour/${random.pick()}.txt">
	<#else>
	</#if>
</@compress>


<@compress single_line=true>
    <#if nutr_n gt 0>
        <#if nutr1_significant == 1 || precise_reporting == 1>
            <#include "/nutrDetails/pre/${random.pick()}.txt"> <#if nutr1_name == 'carbohydrates'> carbohydrate <#else> ${nutr1_name} </#if> <#include "/nutrDetails/middle/${random.pick()}.txt"> is
            <#if low_numeracy == 1>
                ${quantifier.paraphrase(nutr1_val,false)}
            <#else> ${quantifier.scale(nutr1_val, true, false)}
            </#if>
            <#if quantifier.checkIfCorrectAmount(nutr1_val) == 0>
                <#include "/nutrDetails/post/${random.pick()}.txt">
            <#else>.
            </#if>
            <#if nutr1_comp??> <#include "comp/"+nutr1_comp+"/"+random.pick()+".txt"> </#if>
            <#if nutr_n gt 1 && nutr1_food != nutr2_food>
                <#if nutr1_val < 100>
                    <#assign food>${nutr1_food}</#assign>
                    <#assign nutr>${nutr1_name}</#assign>
                    <#include "/nutrFood/deficiency/${random.pick()}.txt">
                <#else>
                    <#if nutr1_val < 100> <#include "/nutrFood/deficiency/${random.pick()}.txt"></#if>
                    </#if>
            </#if>
            <#if report_effects ==1>
                <#if nutr_n gt 1 && nutr1_food != nutr2_food && nutr1_eff != nutr2_eff>
                    <#include "/advEffects/pre/${random.pick()}.txt"> <#if nutr1_name == 'carbohydrates'> carbohydrate <#else> ${nutr1_name} </#if>  <#if nutr1_val gt 100> excess <#else> deficiency </#if> <#include "/advEffects/middle/${random.pick()}.txt"> ${nutr1_eff}.
                </#if>
            </#if>
        </#if>
    </#if>
</@compress>

<@compress single_line=true>
    <#if nutr_n gt 1>
        <#if nutr2_significant == 1 || precise_reporting == 1>
            <#include "/nutrDetails/pre/${random.pick()}.txt"> <#if nutr2_name == 'carbohydrates'> carbohydrate <#else> ${nutr2_name} </#if>  <#include "/nutrDetails/middle/${random.pick()}.txt"> is
            <#if low_numeracy == 1>
                ${quantifier.paraphrase(nutr2_val,false)}
            <#else> ${quantifier.scale(nutr2_val, true, false)}
            </#if>
            <#if quantifier.checkIfCorrectAmount(nutr2_val) == 0>
                <#include "/nutrDetails/post/${random.pick()}.txt">
            <#else>.
            </#if>
            <#if nutr2_comp??> <#include "comp/"+nutr2_comp+"/"+random.pick()+".txt"> </#if>
                <#if nutr1_food != nutr2_food>
                    <#if nutr2_val < 100>
                        <#assign food>${nutr2_food}</#assign>
                        <#assign nutr>${nutr2_name}</#assign>
                        <#include "/nutrFood/deficiency/${random.pick()}.txt">
                        <#else>
                            <#if nutr2_val < 100> <#include "/nutrFood/deficiency/${random.pick()}.txt"></#if>
                    </#if>
                <#else>
                    <#include "/sameFood/${random.pick()}.txt"> "${nutr2_food}".
                    <#if (nutr1_val gt 100 && nutr2_val lt 100)>
                        <#assign val>${nutr2_name}</#assign>
                        <#include "/sameFood/oppositeProblem/${random.pick()}.txt">
                    </#if>
                    <#if (nutr2_val gt 100 && nutr1_val lt 100)>
                        <#assign val>${nutr1_name}</#assign>
                        <#include "/sameFood/oppositeProblem/${random.pick()}.txt">
                    </#if>
                </#if>
            <#if report_effects ==1>
                <#if nutr1_eff == nutr2_eff>
                    <#include "/advEffects/pre/${random.pick()}.txt"> ${nutr1_name}
                    <#if (nutr1_val gt 100 && nutr2_val gt 100) || (nutr1_val lt 100 && nutr2_val lt 100)>
                    <#else>
                        <#if nutr1_val gt 100>
                            excess
                        <#else>
                            deficiency
                        </#if>
                    </#if>

                    and <#if nutr2_name == 'carbohydrates'> carbohydrate <#else> ${nutr2_name} </#if>
                    <#if nutr2_val gt 100>
                        excess <#else>
                        deficiency </#if>
                    <#include "/advEffects/middle/${random.pick()}.txt"> ${nutr2_eff}.
                <#else> <#if nutr1_food != nutr2_food>
                            <#include "/advEffects/pre/${random.pick()}.txt"> <#if nutr2_name == 'carbohydrates'> carbohydrate <#else> ${nutr2_name} </#if>  <#if nutr2_val gt 100> excess <#else> deficiency </#if> <#include "/advEffects/middle/${random.pick()}.txt"> ${nutr2_eff}.
                        <#else> <#include "/advEffects/pre/${random.pick()}.txt"> ${nutr1_name}
                            <#if nutr1_val gt 100>
                                excess
                            <#else> deficiency
                            </#if>
                            <#include "/advEffects/middle/${random.pick()}.txt"> ${nutr1_eff} and ${nutr2_name}
                            <#if nutr2_val gt 100>
                                excess
                            <#else> deficiency
                            </#if> <#include "/advEffects/middle/${random.pick()}.txt"> ${nutr2_eff}.
                        </#if>
                </#if>
            </#if>
        </#if>
    </#if>
</@compress>


<@compress single_line=true>
        ${username}, <#include "/nhs/${random.pick()}.txt">
</@compress>

<#-- ...
<@compress single_line=true>
    <#if nutr_n gt 0 && nutr1_significant == 1>
        ${username}, <#include "/contMatching/${random.pick()}.txt"> ${nutr1_name} <#include "/nutrSuggestion/1/${random.pick()}.txt"> ${nutr1_sugg}.

        <#if nutr_n gt 1 && nutr2_significant == 1> <#include "/misc/conjunction/${random.pick()}.txt"> ${nutr2_name} <#include "/nutrSuggestion/2/${random.pick()}.txt"> ${nutr2_sugg}</#if>.
    </#if>
</@compress>
-->