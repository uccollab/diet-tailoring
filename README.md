# MYFITNESSPAL NLG DIET TAILORING
---
>Communication in healthcare can improve therapy adherence
>and patient engagement. Research into healthcare-oriented Natural
>Language Generation (NLG) systems suggests that tailoring to
>user profile can improve overall effectiveness. However lots of systems
>adopts a single or small group of user profiles, thus overlooking that
>user subsets may have different needs and that these could evolve over
>time. In this paper we conceptualize a framework that can produce customised
>healthcare reports by extracting meaningful data insights and
>producing a final text which varies in content and terminology according
>to the user profile and traits.

---
This repo contains an initial implementation of an NLG tailoring system for healthcare. The system is designed to:  

  - scrape data from MyFitnessPal account
  - filter it based on a user profile
  - and return a text report.

This repo also contains the reference paper.
---
# Requirements
The following instructions are meant for Ubuntu systems, but a quick web search can provide you with instructions on how installing these systems on other OS such as Windows:
  
  
  - Pyhon3 -> for Data Analysis 
    http://ubuntuhandbook.org/index.php/2017/07/install-python-3-6-1-in-ubuntu-16-04-lts/  
moreover you need to install a couple of frameworks and library:  
      
```python
    !pip3 install myfitnesspal
    !pip3 install google-api-python-client
    !pip3 install gspread
    !pip3 install oauth2client
```

  - Java 11 -> for text Realisation
    http://ubuntuhandbook.org/index.php/2018/11/how-to-install-oracle-java-11-in-ubuntu-18-04-18-10/  
    all of the library and dependencies that you need to run the Java code are inside the notebook.


  - Jupyter Notebook -> to run the code in a isolated environment
    https://www.rosehosting.com/blog/how-to-install-jupyter-on-an-ubuntu-16-04-vps/


  - SoS Notebook -> to run multiple languages inside the same notebook 
    https://vatlab.github.io/sos-docs/running.html#content

Suggestion: the jupyter file can be visualised on the repo in a much nicier way by installing the related BitBucket add-on: http://www.legendu.net/en/blog/jupyter-notebook-support-in-bitbucket/

---

### Usage
Once you've cloned the repo you can just open a shell in its folder and run jupyter
      
```sh
$ jupyter notebook
```
The code is meant to be ran all in one time. There's just a couple of things you should replace.
 - date, days, weeks: "_initial_start_date_" refers to the date from which you start parsing user data. 
This are meant to be expressed in YYYY/MM/DD format. So if you want to scrap user data from January
 1 2020 you need:  
      
```python
initial_start_date = date(2020,1,1)
```
Then you must specify the amount of days you're scraping with "_days_". In order to get a week of data you'll do:
            
```python
days = 7
```  
And with "_weeks_" you can specify how many times you want to scrape $_days_ days worth of data. So for example
let's say you want to scrape 4 weeks starting from January 1 2020:
      
```python
initial_start_date = date(2020,1,1)
days = 7
weeks = 4
```   
    
The first Python cell contains, together with some important dicts, some critical folder you will have to provide:

 - **userListPath** : replace this with a path to a .txt that contains the list of your users.  
The file structure should be user/password, each one on a different line. Please consider that this is a  
starting project and this is not a safe way to handle user login. This is currently a work in progress.  

- **userDataPath**: this will be a folder in which the framework will store all of the  
data. You will get a folder for each username, which will contain the user profile and a subfolder for any given date range.
 - **clientSecretPath**: as for now the user profiling is achieved with a Google Form. It is convenient  
as you can instruct GForm to send the data to a Google spreadsheet. In order to parse that you need to create a new project in Google Developer   
Console, enable GDrive API and download a client_secret.json. This prevents unauthorised people from scraping your form without permission.  
Remember to specify in the same line of code your Google Sheet name (**GSheetName**).

 - **templatesPath**: this is the path to the folder in which you have your templates. This repo includes a templates   
 folder with everything you need to get diet reports. Inside it you will also find the FreeMarker templates (.ftl files).


All of these data must be specified inside the Java cells as well (just before the "Realisation" one)